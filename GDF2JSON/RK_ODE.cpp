#include "RK_ODE.h"

RK_ODE::RK_ODE(int Mode, double step) {
	this->_Mode = Mode;
	this->_Step = step;
	_current_time = _start;
	f2 = MatrixXd::Zero(Mode, Mode);
	f1 = MatrixXd::Zero(Mode, Mode);
	f0 = MatrixXd::Zero(Mode, Mode);
	f  = MatrixXd::Zero(Mode, Mode);

	dX = MatrixXd::Zero(Mode, 1);
	X = MatrixXd::Zero(Mode, 1);
}

void RK_ODE::Set_Initial_Value(const MatrixXd &_dX, const MatrixXd& _X) {
	this->dX = _dX;
	this->X = _X;
}

void RK_ODE::Cal_Coefficient(
	MatrixXd(*F2)(double t), 
	MatrixXd(*F1)(double t), 
	MatrixXd(*F0)(double t), 
	MatrixXd(*F)(double t),
	double t)
{
	this->f2 = F2(t);
	this->f1 = F1(t);			
	this->f0 = F0(t);
	this->f = F(t);
}

void RK_ODE::Solve(
	MatrixXd(*F2)(double t),
	MatrixXd(*F1)(double t),
	MatrixXd(*F0)(double t),
	MatrixXd(*F)(double t)
	)
{
	_current_time += _Step;

	this->Cal_Coefficient(F2,F1,F0,F, _current_time);

	MatrixXd K1a = this->Func_f(dX);
	MatrixXd K1b = this->Func_g(X, dX);

	this->Cal_Coefficient(F2, F1, F0, F, _current_time +0.5* _Step);
	MatrixXd K2a = this->Func_f(dX + 0.5 * _Step * K1b);
	MatrixXd K2b = this->Func_g(X + 0.5 * _Step * K1a, dX + 0.5 * _Step * K1b);

	MatrixXd K3a = this->Func_f(dX + 0.5 * _Step * K2b);
	MatrixXd K3b = this->Func_g(X + 0.5 * _Step * K2a, dX + 0.5 * _Step * K2b);

	this->Cal_Coefficient(F2, F1, F0, F, _current_time + _Step);
	MatrixXd K4a = this->Func_f(dX + _Step * K3b);
	MatrixXd K4b = this->Func_g(X + _Step * K3a, dX + _Step * K3b);

	X = X + _Step / 6. * (K1a + 2 * K2a + 2 * K3a + K4a);
	dX = dX + _Step / 6. * (K1b + 2 * K2b + 2 * K3b + K4b);
}


void RK_ODE::WriteMotionAndForce(const char* filename,int Index)
{
	//Index:0-5 for Motion 1-6
	FILE* fp;
	fp = fopen(filename, "a+");
	fprintf(fp, "%6.3e,%6.3e,%6.3e,%6.3e\n",
		_current_time, X(Index, 0), dX(Index, 0), f(Index, 0));
	fclose(fp);
}

