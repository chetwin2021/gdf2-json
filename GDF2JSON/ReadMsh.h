#pragma once
#include <iostream>
#include <iomanip>
#include <cmath>
#include <list>
#include <vector>
#include "Eigen\Dense"
#include "json.hpp"
#include <fstream>



using namespace std;
using namespace Eigen;
using json = nlohmann::json;

class _Panel {
public:
    vector<int> P;                              //point index

    _Panel(int p[4]);
    _Panel();
    _Panel(const _Panel& e);					//copy constructor
    void SetPoint(const int p[4]);
    _Panel & operator =(const _Panel& e);		//overload =

    friend ostream& operator<<( ostream & os,const _Panel & e);//overload <<
    void reverse();                             //reverse the point order

    bool IsNeighbors(_Panel & e,int & count);//判断是s否为邻居，判断是否需要转换法向
};


class ReadMsh
{
public:
    bool IsOpen=false;
	char Description[100];
	int Points_N = 0;
	int Face_N = 0;
    //int **Face;

    int reverse_count=0; //统计翻转计数
    list<_Panel> Face;

	double** Points;

    string Normal_flag;

	ReadMsh(const char* filename);
	~ReadMsh();

    void CorrectNormal();                       // correct the normal to consistent

    void ReverseNormal();                       //reverse normal

    void NormalDirection();          // "in" for inside "out" for outside

	void WriteFilePan(const char* filename);
    void WriteFileGDF(const char* filename);

    void WriteFileJson(const char *filename);
};

