#include"RK_ODE.h"
#include<iostream>
using namespace std;

MatrixXd F2(double t) {
	MatrixXd res = MatrixXd::Ones(1, 1);
	return res;
}

MatrixXd F1(double t) {
	MatrixXd res = MatrixXd::Zero(1, 1);
	return res;
}

MatrixXd F0(double t) {
	MatrixXd res = MatrixXd::Zero(1, 1);
	return res;
}

MatrixXd F(double t) {
	MatrixXd res = MatrixXd::Ones(1, 1);
	res(0, 0) = 1;
	return res;
}

int main() {
	RK_ODE ode(1,0.1);

	ode.X.fill(0.);
	ode.dX.fill(-1.);

	ode.WriteMotionAndForce("results.txt", 0);
	for (int i = 0; i < 100; i++) {
		ode.Solve(F2, F1, F0, F);
		ode.WriteMotionAndForce("results.txt", 0);
	}

	cout << ode._Mode << endl;
}