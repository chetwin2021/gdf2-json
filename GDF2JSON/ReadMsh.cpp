﻿#include "ReadMsh.h"

_Panel::_Panel(int p[4])
{
    for(int i=0;i<4;i++)
        P.push_back(p[i]);
}

_Panel::_Panel(){
    P.assign(4,0);
}

_Panel::_Panel(const _Panel& e)
{
    this->P = e.P;
}

void _Panel::SetPoint(const int p[4])
{
    for (int i = 0; i < 4; i++)
        P[i] = p[i];
}

_Panel& _Panel::operator =(const _Panel& e)
{
    this->P = e.P;
    return *this;
}

void _Panel::reverse()
{
    int tmp;
    for (int i = 0; i < 2; i++) {
        tmp = P[i];
        P[i] = P[3 - i];
        P[3 - i] = tmp;
    }
    //cout<<endl<<"reversed at ["<<this->P[0]<<","<<this->P[1]<<","<<this->P[2]<<","<<this->P[3]<<"]"<<endl<<endl;
}

bool _Panel::IsNeighbors(_Panel & e,int &count){
    int Target;
    vector<int> A,B;
    for (int i = 0; i < 4; i++) {
        Target = this->P[i];
        for (int j = 0; j < 4; j++) {
            if (Target == e.P[j])
            {
                A.push_back(i);
                B.push_back(j);
                break;
            }
        }
    }
    if (A.size() == 2) {
        if ((A[1]-A[0])==1) {
            if ((B[1] - B[0]) == 1) {e.reverse();count++;};
            //if ((B[1] - B[0]) == -1) e.reverse();
            //if ((B[1] - B[0]) == 3) e.reverse();
            if ((B[1] - B[0]) == -3) {e.reverse();count++;};
        }
        else {
            //if ((B[1] - B[0]) == 1) e.reverse();
            if ((B[1] - B[0]) == -1) {e.reverse();count++;};
            //if ((B[1] - B[0]) == 3) e.reverse();
            if ((B[1] - B[0]) == -3) {e.reverse();count++;};
        }
        return true;
    }
    else { return false; }

}

ostream & operator <<( ostream & os,const _Panel& e)
{
    for(int i=0;i<4;i++)
        os <<"P"<<i<<"="<<e.P[i]<<";" ;
    return os;
}


ReadMsh::ReadMsh(const char * filename) {
    FILE* fp;
    fp = fopen(filename, "r");
    if (!fp) cout << "Open file error" << endl;
    else {
        this->IsOpen=true;

        char tmp[100];
        fscanf(fp, "%[^\n]\n", Description);
        fscanf(fp, "%[^\n]\n", tmp);
        fscanf(fp, "%[^\n]\n", tmp);
        fscanf(fp, "%[^\n]\n", tmp);
        int tmp1;
        fscanf(fp, "(%d (%d %d %x %d %d)(\n",
               &tmp1, &tmp1, &tmp1, &Points_N, &tmp1, &tmp1);
        cout << "Point_N:" << Points_N << endl;
        {
            this->Points = new double* [this->Points_N];
            for (int i = 0; i < this->Points_N; ++i) {
                this->Points[i] = new double[3]{};
            }
        }
        for (int i = 0; i < Points_N; ++i) {
            fscanf(fp, "%lf %lf %lf\n", &Points[i][0], &Points[i][1], &Points[i][2]);
        }

        fscanf(fp, "%[^\n]\n", tmp);
        fscanf(fp, "%[^\n]\n", tmp);
        fscanf(fp, "%[^\n]\n", tmp);
        fscanf(fp, "(%d (%d %d %x %d %d)(\n",
               &tmp1, &tmp1, &tmp1, &Face_N, &tmp1, &tmp1);
        cout << "Face_N:" << Face_N << endl;

        int tmp_Face[4]{},tmp_Face_other[3];
        _Panel tmp_Panel;
        for (int i = 0; i < Face_N; ++i) {
            fscanf(fp, "%x %x %x %x %x %x %x\n",
                   &tmp_Face_other[0],
                    &tmp_Face[0], &tmp_Face[1],&tmp_Face[2], &tmp_Face[3],
                    &tmp_Face_other[1],&tmp_Face_other[2]);
            tmp_Panel.SetPoint(tmp_Face);
            Face.push_back(tmp_Panel);
        }
        this->reverse_count=0;
        this->Normal_flag="in";
    }
}

ReadMsh::~ReadMsh() {

    for (int i = 0; i < this->Points_N; ++i) {
        delete[] this->Points[i];
    }
    delete[] this->Points;
}

void ReadMsh::CorrectNormal(){
    list<_Panel> correct,tmp,step;

    tmp.splice(tmp.end(),Face,Face.begin());//将第Face第一个元素剪切至correct

    cout<<"start" <<endl;

    cout<<"Face size : " << Face.size()<<endl;

    //只适用于单连通区域
    int i=0,j=0;int count=1;
    while(this->Face.size()>0){
        for(list<_Panel>::iterator c=tmp.begin(); c!=tmp.end(); ++c){
            i++;
            for(list<_Panel>::iterator t=this->Face.begin(); t!= this->Face.end(); ){
                j++;
                if((*c).IsNeighbors(*t,this->reverse_count)) //判断是否为邻居，同时矫正使得法向一致
                {
                    step.push_back(*t);
                    auto t_tmp = t;
                    t++;
                    this->Face.erase(t_tmp);
                    //step.splice(step.end(),Face,t);
                    cout<<"["<<i<<","<<j<<"] is neighbors"<<"  "<< "Face.left = "
                       <<Face.size() <<endl;
                }
                else {
                    t++;
                }
            }

        }
        //复制tmp至correct中,并清空tmp
        correct.splice(correct.end(),tmp,tmp.begin(),tmp.end());
        //将step压入tmp中,step被清空
        count+=step.size();
        cout<<"current step size : " << step.size()<< "sum : "<< count <<" correct size = "<<correct.size()<<endl;
        tmp.splice(tmp.end(),step,step.begin(),step.end());

    }
    //将最后一次tmp结果存入correct
    correct.splice(correct.end(),tmp,tmp.begin(),tmp.end());

    cout<<"size of corrected "<< correct.size()<<endl;
    cout<<"reverse num  "<< this->reverse_count<<endl;
    //复制correct至Face中，同时清空correct
    this->Face.splice(Face.end(),correct,correct.begin(),correct.end());
}



void ReadMsh::NormalDirection(){
    double down=100;

    Matrix<double,3,1> n13={0,0,0},n24={0,0,0},Norm={0,0,0},Centroid={0,0,0};
    double Costheta = 0, Sintheta = 0;
    double ds=0,P,Fz=0;

    Matrix<double,3,1> P1,P2,P3,P4,tmp;
    for(list<_Panel>::iterator F=Face.begin(); F!=Face.end(); ++F)
    {   P1={Points[(*F).P[0]-1][0],Points[(*F).P[0]-1][1],Points[(*F).P[0]-1][2]};
        P2={Points[(*F).P[1]-1][0],Points[(*F).P[1]-1][1],Points[(*F).P[1]-1][2]};
        P3={Points[(*F).P[2]-1][0],Points[(*F).P[2]-1][1],Points[(*F).P[2]-1][2]};
        P4={Points[(*F).P[3]-1][0],Points[(*F).P[3]-1][1],Points[(*F).P[3]-1][2]};

        //        cout<<"P1 = "<<P1<<endl;
        //        cout<<"P2 = "<<P2<<endl;
        //        cout<<"P3 = "<<P3<<endl;
        //        cout<<"P4 = "<<P4<<endl;

        n13=P3-P1;
        n24=P2-P4;
        Centroid=(P1+P2+P3+P4)/4;
        //        cout<<"n13 = "<<n13<<endl;
        //        cout<<"n24 = "<<n24<<endl;

        Costheta=n13.dot(n24)/(n13.norm()*n24.norm());
        Sintheta = sqrt(1 - pow(Costheta, 2));

        ds=0.5*(n13.norm()*n24.norm()*Sintheta);
        //cout<<"ds = "<<ds<<endl;

        tmp=n13.cross(n24);

        Norm=tmp.normalized();

        P=1000*10*(Centroid(2,0)+down);



        Fz+=P*ds*Norm(2,0);
        //cout<<"Fz = "<<Fz<<endl;

    }
    cout<<"Fz = "<<Fz<<endl;
    if(Fz>0) this->Normal_flag="in";
    else this->Normal_flag="out";
    cout<<"current normal = "<<this->Normal_flag<<endl;
}

void ReadMsh::ReverseNormal(){
    for(list<_Panel>::iterator F=Face.begin(); F!=Face.end(); ++F){
        (*F).reverse();
    }
    if(this->Normal_flag=="in") this->Normal_flag="out";
    else{
        this->Normal_flag="in";
    }
    cout<<"Normal_flag" << Normal_flag <<endl;
}


void ReadMsh::WriteFilePan(const char* filename) {
    FILE* fp;
    fp = fopen(filename, "w");
    fprintf(fp, "Total numbers of points and panels\n");
    fprintf(fp, "%10d   %5d\n", Points_N, Face_N);
    fprintf(fp, "Points and the conectivities\n");
    fprintf(fp,"ZONE T = ""Panel"", N = %d, E = %d, F = FEPOINT, ET = QUADRILATERAL\n", Points_N, Face_N);
    for (int i = 0; i < Points_N; ++i) {
        fprintf(fp, "%15.6f %15.6f %15.6f \n", Points[i][0], Points[i][1], Points[i][2]);
    }

    for(list<_Panel>::iterator F=Face.begin(); F!=Face.end(); ++F)
    {
        fprintf(fp, "%5d %5d %5d %5d \n", (*F).P[0], (*F).P[1], (*F).P[2], (*F).P[3]);
    }
    fclose(fp);

    cout << "Output " << "[" << filename << "]" << " successfully" << endl;
}

void ReadMsh::WriteFileGDF(const char* filename) {
    FILE* fp;
    fp = fopen(filename, "w");
    fprintf(fp, "GDF file generated by (%s) and Transform by ReadMsh\n",Description);
    double LengthScale = 1., Grivaty = 9.806650;
    fprintf(fp, "%10f   %5f\n", LengthScale, Grivaty);
    int center[2] = {};
    fprintf(fp, "%10d   %5d\n", center[0], center[1]);

    fprintf(fp, "%15d\n", Face_N);

    int Index=0;

    for(list<_Panel>::iterator F=Face.begin(); F!=Face.end(); ++F)
    {
        for (int j = 0; j < 4 ; ++j) {
            Index = (*F).P[j]-1;
            fprintf(fp, "%15.6f %15.6f %15.6f\n", Points[Index][0], Points[Index][1], Points[Index][2]);
        }
    }
    fclose(fp);

cout << "Output " << "[" << filename << "]" << " successfully" << endl;
}

//json object export function 
void Jout(const json& in, const char* name) {
    std::ofstream f_out(name);
    f_out << in.dump(4) << std::endl;
    f_out.close();
    cout << "write " << name << " file successfully" << endl;
}

void ReadMsh::WriteFileJson(const char* filename)
{
    json j;
    j["Panel Number"] = this->Face_N;
    j["Description"] = this->Description;
    j["Normal"] = this->Normal_flag;

    //char name[100];sprintf(name, "Panel_index %05d", i);
    int i = 0, index;
    //double Point[4][3]{};
    std::vector<std::vector<double>> P;
    std::vector<double> p;
    for (auto F = this->Face.begin(); F != this->Face.end(); F++) {
        for (int j = 0; j < 4; ++j) {
            index = (*F).P[j] - 1;
            p.clear();
            p.push_back(Points[index][0]);
            p.push_back(Points[index][1]);
            p.push_back(Points[index][2]);
            //Point[j][0] = Points[index][0];
            //Point[j][1]=Points[index][1];
            //Point[j][2]=Points[index][2];
        }
       // i++;
        P.push_back(p);
    }
	
	j["Point"] = (P);
    Jout(j,filename); 
}

//Transform

