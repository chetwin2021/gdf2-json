#pragma once

#include "Eigen\Dense"
#include "json.hpp"


using MatrixXd = Eigen::MatrixXd;

/* Solver EQ:
	f2(t)*X''(t)+f1(t)*X'(t)+f0*X(t)==F(t)
*/

class RK_ODE
{
public:
	int _Mode = 1;
	double _Step=0.1;
	double _start = 0;
	double _current_time = 0;
	//Store Coefficient
	MatrixXd f2;
	MatrixXd f1;
	MatrixXd f0;
	MatrixXd f;
	//results
	MatrixXd dX;
	MatrixXd X;

	RK_ODE(int Mode,double step);

	void Set_Initial_Value(const MatrixXd& _dX, const MatrixXd& _X);

	void Cal_Coefficient(
		MatrixXd(*F2)(double t),
		MatrixXd(*F1)(double t),
		MatrixXd(*F0)(double t),
		MatrixXd(*F)(double t),
		double t
	);

	MatrixXd Func_f(MatrixXd b0) { return b0; }
	MatrixXd Func_g(MatrixXd a0, MatrixXd b0) {
		return f2.inverse() * ( - 1. * f1 * b0 - f0 * a0 + f);
	}

	void Solve(
		MatrixXd(*F2)(double t),
		MatrixXd(*F1)(double t),
		MatrixXd(*F0)(double t),
		MatrixXd(*F)(double t)
		);

	void WriteMotionAndForce(const char* filename,int Index);

};

